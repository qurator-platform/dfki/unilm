FROM python:3.6

WORKDIR /app

RUN git clone https://github.com/microsoft/unilm.git
RUN cd unilm/layoutlm
#RUN pip install pytorch==1.4.0 cudatoolkit=10.1 -c pytorch
RUN pip install torch torchvision

RUN git clone https://github.com/NVIDIA/apex
RUN cd /app/apex
RUN pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" /app/apex
RUN pip install .

CMD ["python", "app.py"]
